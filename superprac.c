#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

const double A = 3.0;
const double B = 3.0;

int NX, NY;                 
double hx, hy;   

#define Print
#define Step 10

#define TRUE ((int) 1)
#define FALSE ((int) 0)

#define Pow2(x) ((x)*(x))

#define x(i) ((i)*hx)
#define y(j) ((j)*hy)

#define LeftPart(P,i,j)\
((-(P[NX*(j)+i+1]-P[NX*(j)+i])/hx+(P[NX*(j)+i]-P[NX*(j)+i-1])/hx)/hx+\
 (-(P[NX*(j+1)+i]-P[NX*(j)+i])/hy+(P[NX*(j)+i]-P[NX*(j-1)+i])/hy)/hy)


double BoundaryValue(double x, double y)
{
    return log(1.0 + x*y);
}

void RightPart(double *rhs)
{
    int i, j;
    
    #pragma omp parallel for
    for (j = 0; j < NY; j++)
        for (i = 0; i < NX; i++)
            rhs[j*NX + i] = (x(i)*x(i) + y(j)*y(j)) / Pow2(1.0 + x(i)*y(j));
}


int IsPower(int Number)
{
    unsigned int M;
    int p;
    
    if (Number <= 0)
        return(-1);
    
    M = Number; 
    p = 0;

    while (M % 2 == 0){
        ++p;
        M = M >> 1;
    }

    if ((M >> 1) != 0)
        return(-1);
    else
        return(p);
}

int SplitFunction(int N0, int N1, int p)
{
    float n0, n1;
    int p0, i;

    n0 = (float)N0; 
    n1 = (float)N1;
    p0 = 0;

    for (i = 0; i < p; i++){
        if (n0 > n1){
            n0 = n0 / 2.0;
            ++p0;
        }
        else
            n1 = n1 / 2.0;
    }
    
    return(p0);
}

void FivePointSchema(double *data, int x_start, int x_end, int y_start, int y_end, int left, int right, int up, int down)
{
    if (left != MPI_PROC_NULL){
        int size = y_end - y_start + 1;
	    double *sendbuf = (double *)malloc(size * sizeof(double));
	    double *recvbuf = (double *)malloc(size * sizeof(double));
	    
	    #pragma omp parallel for
	    for (int j = y_start; j <= y_end; j++)
	        sendbuf[j - y_start] = data[NX*j + x_start];
	    
	    MPI_Status status;
	    
	    MPI_Sendrecv(sendbuf, size, MPI_DOUBLE, left, 0, recvbuf, size, MPI_DOUBLE, left, 0, MPI_COMM_WORLD, &status);
	    
	    #pragma omp parallel for
	    for (int j = y_start; j <= y_end; j++)
	        data[NX*j + x_start - 1] = recvbuf[j - y_start];
	    
	    free(sendbuf);
	    free(recvbuf);
	}
    
    if (right != MPI_PROC_NULL){
        int size = y_end - y_start + 1;
	    double *sendbuf = (double *)malloc(size * sizeof(double));
	    double *recvbuf = (double *)malloc(size * sizeof(double));
	    
	    #pragma omp parallel for
	    for (int j = y_start; j <= y_end; j++)
	        sendbuf[j - y_start] = data[NX*j + x_end];
	    
	    MPI_Status status;
	    
	    MPI_Sendrecv(sendbuf, size, MPI_DOUBLE, right, 0, recvbuf, size, MPI_DOUBLE, right, 0, MPI_COMM_WORLD, &status);
	    
	    #pragma omp parallel for
	    for (int j = y_start; j <= y_end; j++)
	        data[NX*j + x_end + 1] = recvbuf[j - y_start];
	    
	    free(sendbuf);
	    free(recvbuf);
    }

    if (up != MPI_PROC_NULL){
        int size = x_end - x_start + 1;
	    double *sendbuf = (double *)malloc(size * sizeof(double));
	    double *recvbuf = (double *)malloc(size * sizeof(double));
	    
	    #pragma omp parallel for
	    for (int i = x_start; i <= x_end; i++)
	        sendbuf[i - x_start] = data[NX*y_start + i];
	    
	    MPI_Status status;
	    
	    MPI_Sendrecv(sendbuf, size, MPI_DOUBLE, up, 0, recvbuf, size, MPI_DOUBLE, up, 0, MPI_COMM_WORLD, &status);
	    
	    #pragma omp parallel for
	    for (int i = x_start; i <= x_end; i++)
	        data[NX*(y_start - 1) + i] = recvbuf[i - x_start];
	    
	    free(sendbuf);
	    free(recvbuf);
	}
    
    if (down != MPI_PROC_NULL){
        int size = x_end - x_start + 1;
	    double *sendbuf = (double *)malloc(size * sizeof(double));
	    double *recvbuf = (double *)malloc(size * sizeof(double));
	    
	    #pragma omp parallel for
	    for (int i = x_start; i <= x_end; i++)
	        sendbuf[i - x_start] = data[NX*y_end + i];
	    
	    MPI_Status status;
	    
	    MPI_Sendrecv(sendbuf, size, MPI_DOUBLE, down, 0, recvbuf, size, MPI_DOUBLE, down, 0, MPI_COMM_WORLD, &status);
	    
	    #pragma omp parallel for
	    for (int i = x_start; i <= x_end; i++)
	        data[NX*(y_end + 1) + i] = recvbuf[i - x_start];
	    
	    free(sendbuf);
	    free(recvbuf);
	}
}

void CollectData(int rank, int procs, double *SolVect, int x_start, int x_end, int y_start, int y_end)
{
    int xcoords[2], ycoords[2];
    int size = NX*NY;
    
    if (rank == 0){
        MPI_Status status;
        double *recvSolVect = (double *)malloc(size * sizeof(double));

        for (int p = 1; p < procs; p++){
            MPI_Recv(xcoords, 2, MPI_INT, p, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(ycoords, 2, MPI_INT, p, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(recvSolVect, size, MPI_DOUBLE, p, 0, MPI_COMM_WORLD, &status);
            
            #pragma omp parallel for
            for (int j = ycoords[0]; j <= ycoords[1]; j++)
                for (int i = xcoords[0]; i <= xcoords[1]; i++)
                    SolVect[NX*j + i] = recvSolVect[NX*j + i];
        }

        free(recvSolVect);
    }
    else{
        MPI_Request requests[3];
        
        xcoords[0] = x_start;
        xcoords[1] = x_end;
        ycoords[0] = y_start;
        ycoords[1] = y_end;
        
        MPI_Isend(xcoords, 2, MPI_INT, 0, 0, MPI_COMM_WORLD, &requests[0]);
        MPI_Isend(ycoords, 2, MPI_INT, 0, 0, MPI_COMM_WORLD, &requests[1]);
        MPI_Isend(SolVect, size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &requests[2]);
        
        MPI_Waitall(3, requests, MPI_STATUS_IGNORE);
    }
}


int main(int argc, char * argv[])
{
    int ProcNum, rank;          // the number of processes and rank in communicator
    int power, py, px;          // ProcNum = 2^(power), power splits into sum py + px
    int dims[2];                // dims[0] = 2^py, dims[1] = 2^px (--> M = dims[0]*dims[1])
    int ny, nx, ky, kx;         // NY = ny*dims[0] + ky, NX = nx*dims[1] + kx
    int Coords[2];              // the process coordinates in the cartesian topology created for mesh
    
    MPI_Comm Grid_Comm;         // this is a handler of a new communicator
    const int ndims = 2;        // the number of a process topology dimensions
    int periods[2] = {0, 0};    // it is used for creating processes topology
    int left, right, up, down;  // the neighbours of the process
    
    // MPI Library is being activated ...
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    double starttime, endtime;
    starttime = MPI_Wtime();
    
    double *SolVect;                    // the solution array
    double *ResVect;                    // the residual array
    double *BasisVect;                  // the vector of A-orthogonal system in CGM
    double *RHS_Vect;                   // the right hand side of Puasson equation
    double sp, alpha, tau, NewValue;    // auxiliary values
    int counter;                        // the current iteration number
    const double eps = 1e-4;
    double diff = 1, val_diff, sum;
    
    int i, j;
    char str[127];
    FILE *fp;
    
    if (argc != 3){
        if (rank == 0){
            printf("Wrong number of parameters in command line.\n"
                   "Usage: <ProgName> <Nodes number on (0x) axis> <Nodes number on (0y) axis>\n");
        }
        
        MPI_Finalize();
        return(1);
    }
    
    NX = atoi(argv[1]);
    NY = atoi(argv[2]);
    hx = A / (NX-1);    
    hy = B / (NY-1);
    
    if ((NY <= 0) || (NX <= 0)){
        if (rank == 0)
            printf("The first and the second arguments (mesh numbers) should be positive.\n");
        
        MPI_Finalize();
        return(2);
    }
    
    if ((power = IsPower(ProcNum)) < 0){
        if (rank == 0)
            printf("The number of procs must be a power of 2.\n");
        
        MPI_Finalize();
        return(3);
    }
    
    py = SplitFunction(NY, NX, power);
    px = power - py;
    
    dims[0] = (unsigned int)1 << py;    
    dims[1] = (unsigned int)1 << px;
    
    ny = NY >> py;                      
    nx = NX >> px;
    
    ky = NY - dims[0] * ny;             
    kx = NX - dims[1] * nx;
    
    #ifdef Print
    if (rank == 0){
        printf("The number of processes ProcNum = 2^%d. It is split into %d x %d processes.\n"
               "The number of nodes NX = %d, NY = %d. Blocks B(i,j) have size:\n", power, dims[0], dims[1], NX, NY);
        
        if ((ky > 0) && (kx > 0))
            printf("-->\t %d x %d iff i = 0 .. %d, j = 0 .. %d;\n", ny+1, nx+1, ky-1, kx-1);
        if (kx > 0)
            printf("-->\t %d x %d iff i = %d .. %d, j = 0 .. %d;\n", ny, nx+1, ky, dims[0]-1, kx-1);
        if (ky > 0)
            printf("-->\t %d x %d iff i = 0 .. %d, j = %d .. %d;\n", ny+1, nx, ky-1, kx, dims[1]-1);
        
        printf("-->\t %d x %d iff i = %d .. %d, j = %d .. %d.\n", ny, nx, ky, dims[0]-1, kx, dims[1]-1);
    }
    #endif
    
    // the cartesian topology of processes is being created ...
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, TRUE, &Grid_Comm);
    MPI_Comm_rank(Grid_Comm, &rank);
    MPI_Cart_coords(Grid_Comm, rank, ndims, Coords);
    
    if (Coords[0] < ky) ++ny;
    if (Coords[1] < kx) ++nx;
    
    int y_start = Coords[0] < ky ? Coords[0] * ny : ky * (ny + 1) + (Coords[0] - ky) * ny;
    int y_end = y_start + ny - 1;
    
    int x_start = Coords[1] < kx ? Coords[1] * nx : kx * (nx + 1) + (Coords[1] - kx) * nx;
    int x_end = x_start + nx - 1;
    
    MPI_Cart_shift(Grid_Comm, 1, 1, &left, &right);
    MPI_Cart_shift(Grid_Comm, 0, 1, &up, &down);
    
    if (left == MPI_PROC_NULL) 
        x_start++;
    if (right == MPI_PROC_NULL) 
        x_end--;
    
    if (up == MPI_PROC_NULL) 
        y_start++;
    if (down == MPI_PROC_NULL) 
        y_end--;
    
    if (rank == 0){
        sprintf(str, "Puasson_CGM_%dx%d.log", NX, NY);
        fp = fopen(str, "w");
        fprintf(fp, "The Domain: [0,%f]x[0,%f], number of points: N[0,A] = %d, N[0,B] = %d.\n", A, B, NX, NY);
    }
    
    SolVect = (double *)malloc(NX*NY * sizeof(double));
    ResVect = (double *)malloc(NX*NY * sizeof(double));
    RHS_Vect = (double *)malloc(NX*NY * sizeof(double));
    
    // Initialization of Arrays
    memset(SolVect, 0, NX*NY * sizeof(double));
    memset(ResVect, 0, NX*NY * sizeof(double));
    RightPart(RHS_Vect);
    
    #pragma omp parallel for
    for (i = 0; i < NX; i++){
        SolVect[i] = BoundaryValue(x(i), 0.0);
        SolVect[NX*(NY - 1) + i] = BoundaryValue(x(i), B);
    }
    
    #pragma omp parallel for
    for (j = 0; j < NY; j++){
        SolVect[NX*j] = BoundaryValue(0.0, y(j));
        SolVect[NX*j + (NX - 1)] = BoundaryValue(A, y(j));
    }
    
    // Steep descent iteration begin ...
    #ifdef Print
    if (rank == 0)
        printf("\nSteep descent iteration begin ...\n");
    #endif
    
    // The residual vector r(k) = Ap(k)-f is calculating ...
    #pragma omp parallel for
    for (j = y_start; j <= y_end; j++)
        for (i = x_start; i <= x_end; i++)
            ResVect[NX*j + i] = LeftPart(SolVect, i, j) - RHS_Vect[NX*j + i];
    
    // The value of product (r(k),r(k)) is calculating ...
    sp = 0.0;
    #pragma omp parallel for reduction(+:sp)
    for (j = y_start; j <= y_end; j++)
        for (i = x_start; i <= x_end; i++)
            sp += ResVect[NX*j + i] * ResVect[NX*j + i] * hx * hy;
    
    MPI_Allreduce(&sp, &tau, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    // The value of product sp = (Ar(k),r(k)) is calculating ...
    FivePointSchema(ResVect, x_start, x_end, y_start, y_end, left, right, up, down);
    
    sp = 0.0;
    #pragma omp parallel for reduction(+:sp)
    for (j = y_start; j <= y_end; j++)
        for (i = x_start; i <= x_end; i++)
            sp += LeftPart(ResVect, i, j)*ResVect[NX*j + i] * hx * hy;
    
    MPI_Allreduce(&sp, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    sp = sum;
    tau = tau / sp;
    
    // The p(k+1) is calculating ...
    #pragma omp parallel for
    for (j = y_start; j <= y_end; j++)
        for (i = x_start; i <= x_end; i++)
            SolVect[NX*j + i] = SolVect[NX*j + i] - tau*ResVect[NX*j + i];
    
    // The end of steep descent iteration
    
    BasisVect = ResVect;    // g(0) = r(k-1)
    ResVect = (double *)malloc(NX*NY * sizeof(double));
    memset(ResVect, 0, NX*NY * sizeof(double));
    
    // CGM iterations begin ...
    // sp == (Ar(k-1),r(k-1)) == (Ag(0),g(0)), k=1
    #ifdef Print
    if (rank == 0)
        printf("\nCGM iterations begin ...\n");
    #endif
    
    counter = 0;
    while (diff > eps){
        counter++;
        
        // The residual vector r(k) is calculating ...
        FivePointSchema(SolVect, x_start, x_end, y_start, y_end, left, right, up, down);
        
        #pragma omp parallel for
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++)
                ResVect[NX*j + i] = LeftPart(SolVect, i, j) - RHS_Vect[NX*j + i];
        
        // The value of product (Ar(k),g(k-1)) is calculating ...
        FivePointSchema(ResVect, x_start, x_end, y_start, y_end, left, right, up, down);
        
        alpha = 0.0;
        #pragma omp parallel for reduction(+:alpha)
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++)
                alpha += LeftPart(ResVect, i, j)*BasisVect[NX*j + i] * hx * hy;
        
        MPI_Allreduce(&alpha, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        alpha = sum / sp;
        
        // The new basis vector g(k) is being calculated ...
        #pragma omp parallel for
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++)
                BasisVect[NX*j + i] = ResVect[NX*j + i] - alpha*BasisVect[NX*j + i];
        
        // The value of product (r(k),g(k)) is being calculated ...
        tau = 0.0;
        #pragma omp parallel for reduction(+:tau)
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++)
                tau += ResVect[NX*j + i] * BasisVect[NX*j + i] * hx * hy;
        
        MPI_Allreduce(&tau, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        tau = sum;
        
        // The value of product sp = (Ag(k),g(k)) is being calculated ...
        FivePointSchema(BasisVect, x_start, x_end, y_start, y_end, left, right, up, down);
        
        sp = 0.0;
        #pragma omp parallel for reduction(+:sp)
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++)
                sp += LeftPart(BasisVect, i, j)*BasisVect[NX*j + i] * hx * hy;
        
        MPI_Allreduce(&sp, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        sp = sum;
        tau = tau / sp;
        
        // The p(k+1) is being calculated ...
        diff = 0.0;
        #pragma omp parallel for reduction(+:diff)
        for (j = y_start; j <= y_end; j++)
            for (i = x_start; i <= x_end; i++){
                NewValue = SolVect[NX*j + i] - tau*BasisVect[NX*j + i];
                
                val_diff = fabs(NewValue - SolVect[NX*j + i]);
                diff += val_diff*val_diff*hx*hy;
                
                SolVect[NX*j + i] = NewValue;
            }
        
        MPI_Allreduce(&diff, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        diff = sqrt(sum);
        
        if (counter%Step == 0){
            if (rank == 0)
                printf("The %d iteration of CGM method has been carried out.\n", counter);
            
            #ifdef Print
            if (rank == 0)
                fprintf(fp, "\nThe iteration %d of conjugate gradient method has been finished.\n"
                            "The difference is %f.\n", counter, diff);
            #endif
        }
    }
    
    CollectData(rank, ProcNum, SolVect, x_start, x_end, y_start, y_end);
    
    // The end of CGM iterations
    
    endtime = MPI_Wtime();
    
    // Printing some results ...
    if (rank == 0){
        fprintf(fp, "\nThe %d iterations are carried out.\n\n", counter);
        fclose(fp);
        
        sprintf(str, "Puasson_CGM_%dx%d.dat", NX, NY);
        fp = fopen(str, "w");
        
        for (j = 0; j < NY; j++){
            for (i = 0; i < NX; i++)
                fprintf(fp, "%f %f %f\n", x(i), y(j), SolVect[NX*j + i]);
        }
        fclose(fp);
        
        printf("\nYou can draw it by gnuplot by the command: splot '%s'\n\n", str);
        printf("Execution time: %f\n", endtime - starttime);
    }
    
    free(SolVect); free(ResVect);
    free(BasisVect); free(RHS_Vect);
    
    MPI_Finalize();
    // The end of MPI session ...
    
    return(0);
}
